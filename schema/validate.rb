#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'
require 'json-schema'

current_dir = File.dirname(__FILE__)
schema_file = File.join(current_dir, 'schema.json')
$schema = JSON.parse(File.read(schema_file))

def validation_errors(yaml_file)
  err = []
  begin
    yaml_dict = YAML.load_file(yaml_file)
    err = JSON::Validator.fully_validate($schema, yaml_dict.to_json)
    # validate whether path to avisory is consistent with package slug
    target_file = "#{File.join(yaml_dict['package_slug'], yaml_dict['identifier'])}.yml"
    unless yaml_file == target_file
      err << "detected inconcistencies between package slug and file for #{yaml_file}"
    end
  rescue StandardError => e
    err << e.message
  end
  err
end

def obtain_yaml_files(path)
  File.directory?(path) ? Dir.glob("#{path}/**/*.yml") : [path]
end

unless ARGV.any?
  puts "#{$PROGRAM_NAME} <path0> <path1> ... <pathN>"
  puts 'path can be a yaml file and/or a directory'
  exit(1)
end

# make sure all provided paths do exist
if ARGV.reject { |path| File.exist?(path) }.any?
  puts 'Not all provided paths do exist'
  exit(1)
end

ARGV.flat_map { |path| obtain_yaml_files(path) }.each do |yaml_file|
  next if %w[unknown].include?(File.basename(yaml_file, '.yml'))

  errors = validation_errors(yaml_file)
  if errors.any?
    puts "#{yaml_file} is invalid: #{errors.join('; ')}"
    exit(1)
  end
end

puts 'All yaml files are valid'
exit(0)
