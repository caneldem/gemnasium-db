<!-- Follow the contributing guide https://gitlab.com/gitlab-org/security-products/gemnasium-db/blob/master/CONTRIBUTING.md#submit-a-vulnerability -->
<!-- Title must feature both the advisory identifier and the package name, like this: Add CVE-2019-XYZ to Package-xyz -->
<!-- Please provide a link to a security advisory, to the affected code (mentioning the security flaw) or the fixed code.  -->

To be checked:

* [ ] `identifier` should be the CVE id when it exists.
* [ ] `package_slug` refers to a package listed on [Gemnasium](https://deps.sec.gitlab.com/explore).
* [ ] `title` is a short description. It does not contain the package name.
* [ ] `description` must not contain an overview of the package, fixed versions, affected versions, solution or links.
      It leverages the Markdown syntax.
* [ ] `date` is the date on which the advisory was made public.
* [ ] `not_impacted` lists old versions that are not impacted, if any, the fixed versions.
* [ ] `solution` tells how to remediate the vulnerability.
* [ ] `urls` must contain URLs specific to the vulnerability, not URLs generic to the package itself.
* [ ] If all checkboxes up to this point are checked, add label ~advisory::publishable. In case the second checkbox is uncheked (i.e., `package_slug` does not to refer to a package listed on [Gemnasium](https://deps.sec.gitlab.com/explore)), add the label ~"advisory::unpublishable".

/label ~"group::composition analysis" ~"devops::secure" ~"vulnerability database" ~feature
