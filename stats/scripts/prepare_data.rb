#!/usr/bin/env ruby
# frozen_string_literal: true

require 'yaml'
require 'csv'
require 'git'
require 'time'
require 'open-uri'

def download_file(url,file)
  open(file, 'wb') do |file|
    file << open(url).read
  end
end

unless ARGV.size == 2
  puts "usage ./prepare_data.rb <path-to-gemnasium-db> <outdir>"
  exit(1)
end

unless ARGV.reject { |arg| Dir.exist?(arg) }.empty?
  puts "#{ARGV[0]} does not exist"
  exit(1)
end

repo = ARGV[0]
outdir = ARGV[1]

rows = []
rows << ['file', 'pubdate', 'mergedate', 'delta', 'packagetype']
# compute statistics about creation dates/times
Dir.chdir(repo) {
  g = Git.open('.')
  Dir.glob("**/*.yml").each do |file|
    yaml_dict = YAML.load_file(file)
    identifier = File.join(yaml_dict['package_slug'], yaml_dict['identifier'])
    next unless yaml_dict.key?('pubdate')

    cve_creation_date = yaml_dict['pubdate'].to_s
    advisory_merge_date = g.log.object(file).last.date.strftime("%Y-%m-%d").to_s
    delta = ((Time.parse(advisory_merge_date.strip) - Time.parse(cve_creation_date.strip)) / (3600 * 24)).round
    rows << [identifier, cve_creation_date, advisory_merge_date, delta, yaml_dict['package_slug'].split('/').first ]
  end
}

File.write(File.join(outdir, "data.csv"), rows.map(&:to_csv).join)

# download nvd data
file_id = '1gN-p-nB_BoqZ48T79llGI3p-Lg1biO6Q'
download_file("https://docs.google.com/uc?export=download&id=#{file_id}", File.join(outdir, "nvd.csv"))
